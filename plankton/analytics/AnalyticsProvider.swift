//
// Created by functory on 05/07/2022.
//

import Foundation

protocol AnalyticsProvider {
    
    var plistKey: String { get set }
    
    var isInitialized: Bool { get set }

    func initialize(_ key: String, _ appId: String, completion: @escaping () -> ())

    func setUserProperty(_ key: String, _ value: String)

    func logEvent(_ eventName: String, _ params: Dictionary<String, Any>)
    
    func adsAvailable(_ adType: String, _ placement: String, _ isLoaded: Bool)

    func adsStarted(_ adType: String, _ placement: String, _ isStarted: Bool)

    func adsWatch(_ adType: String, _ placement: String, _ result: AdWatchState)
}

extension AnalyticsProvider {
        
    func initialize(_ key: String, _ appId: String, completion: @escaping () -> ()) {}

    func setUserProperty(_ key: String, _ value: String) {}

    func adsAvailable(_ adType: String, _ placement: String, _ isLoaded: Bool) {}

    func adsStarted(_ adType: String, _ placement: String, _ isStarted: Bool) {}

    func adsWatch(_ adType: String, _ placement: String, _ result: AdWatchState) {}
}
