//
// Created by Funtory on 09/07/2022.
//

import YandexMobileMetrica
import Foundation


class AppMetricaAnalytics: AnalyticsProvider, Injectable {
    
    @Inject private var runtimeInfoManager: RuntimeInfoManager

    private let EVENT_ADS_AVAILABLE = "video_ads_available"
    private let EVENT_ADS_STARTED = "video_ads_started"
    private let EVENT_ADS_WATCH = "video_ads_watch"

    private let PARAM_AD_TYPE = "ad_type"
    private let PARAM_PLACEMENT = "placement"
    private let PARAM_RESULT = "result"
    private let PARAM_CONNECTION = "connection"
    
    private let SESSION_TIMEOUT_SECONDS: UInt = 300
    
    private var levelStartTimestamp: Int64? = nil

    var isInitialized: Bool = false

    var plistKey: String = "Yandex"

    func initialize(_ key: String, _ appId: String, completion: @escaping () -> ()) {
        if isInitialized { return }
        
        LogHelper.d("Initializing AppMetricaAnalytics")

        let configuration = YMMYandexMetricaConfiguration.init(apiKey: key)
        configuration?.locationTracking = false
        configuration?.sessionTimeout = SESSION_TIMEOUT_SECONDS
        configuration?.handleFirstActivationAsUpdate = !isNewUser()
        configuration?.logs = true
        
        YMMYandexMetrica.activate(with: configuration!)

        isInitialized = true
        completion()
    }

    func adsAvailable(_ adType: String, _ placement: String, _ isLoaded: Bool) {
        logEvent(EVENT_ADS_AVAILABLE, [
            PARAM_AD_TYPE: adType.lowercased(),
            PARAM_PLACEMENT: placement,
            PARAM_RESULT: isLoaded ? "success" : "not_available",
            PARAM_CONNECTION: runtimeInfoManager.internetState.toBoolean()
        ])
    }

    func adsStarted(_ adType: String, _ placement: String, _ isStarted: Bool) {
        logEvent(EVENT_ADS_STARTED, [
            PARAM_AD_TYPE: adType.lowercased(),
            PARAM_PLACEMENT: placement,
            PARAM_RESULT: isStarted ? "start" : "fail",
            PARAM_CONNECTION: runtimeInfoManager.internetState.toBoolean()
        ])
    }

    func adsWatch(_ adType: String, _ placement: String, _ result: AdWatchState) {
        logEvent(EVENT_ADS_WATCH, [
            PARAM_AD_TYPE: adType.lowercased(),
            PARAM_PLACEMENT: placement,
            PARAM_RESULT: result.rawValue,
            PARAM_CONNECTION: runtimeInfoManager.internetState.toBoolean()
        ])
    }

    
    func logEvent(_ eventName: String, _ params: Dictionary<String, Any>) {
        LogHelper.d("AppMetrica LogEvent called ==> event: \(eventName), params: \(params)")
        YMMYandexMetrica.reportEvent(eventName, parameters: params)
    }
    
    private func isNewUser() -> Bool {
        return runtimeInfoManager.playerInfo.sessionNumber == 1
    }
    
    private func calculateLevelTime() -> Int64 {
        if levelStartTimestamp == nil {
            return 0
        } else {
            return Int64(Date().timeIntervalSince1970) - levelStartTimestamp!
        }
    }
}
