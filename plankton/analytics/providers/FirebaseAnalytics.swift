//
// Created by Funtory on 06/07/2022.
//

import FirebaseAnalytics

class FirebaseAnalytics: AnalyticsProvider, Injectable {
        
    var isInitialized = true
    
    var plistKey: String = "Firebase"

    public func setUserProperty(_ key: String, _ value: String){
        Analytics.setUserProperty(value, forName: key)
    }

//    public func logEvent(_ eventName: String, _ paramsJson: String){
//        logEvent(eventName, try! Utils.jsonToDict(paramsJson))
//    }

    func logEvent(_ eventName: String, _ params: Dictionary<String, Any>) {
        LogHelper.d("Firebase LogEvent called ==> event: \(eventName), params: \(params)")
        Analytics.logEvent(eventName, parameters: params)
    }
}
