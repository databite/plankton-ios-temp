//
//  AppsFlyerAnalytics.swift
//  plankton
//
//  Created by Funtory on 05/07/2022.
//

import AppsFlyerLib
import AppsFlyerAdRevenue

class AppsFlyerAnalytics: AnalyticsProvider, Injectable {
    
    private var ATT_WAIT_TIMEOUT = 60
    
    var isInitialized: Bool = false
    
    var plistKey: String = "AppsFlyer"

    func initialize(_ key: String, _ appId: String, completion: @escaping () -> ()) {
        if isInitialized {return}
        
        LogHelper.d("Initializing AppsFlyerAnalytics")
        AppsFlyerLib.shared().appsFlyerDevKey = key
        AppsFlyerLib.shared().appleAppID = appId
        
        if #available(iOS 14.5, *) {
            AppsFlyerLib.shared().waitForATTUserAuthorization(timeoutInterval: TimeInterval(ATT_WAIT_TIMEOUT))
        }
        
//        AppsFlyerLib.shared().isDebug = true
        
        AppsFlyerLib.shared().start(completionHandler: { (dictionary, error) in
            LogHelper.d("AppsFlyer start completed.")
            if (error != nil){
                LogHelper.d("Appsflyer Error in start: \(error.debugDescription)")
                return
            } else {
                LogHelper.d("Appsflyer Dict: \(String(describing: dictionary))" )
                self.isInitialized = true
                completion()
                return
            }
        })
        
        AppsFlyerAdRevenue.start()
    }

    func logEvent(_ eventName: String, _ params: Dictionary<String, Any>) {}
    
    func logRevenue(adType: String, adUnitId: String, adSourceName: String?, revenueValue: NSDecimalNumber, currencyCode: String, mediation: String = "admob"){
        LogHelper.d("AppsFlyer logging revenue. isInitialized: \(isInitialized).")
        if !isInitialized { return }
        
        LogHelper.d("AdType: \(adType), UnitId: \(adUnitId), SourceName: \(adSourceName), Revenue: \(revenueValue) \(currencyCode)")
        
        let adRevenueParams:[AnyHashable: Any] = [
            kAppsFlyerAdRevenueAdUnit : adUnitId,
            kAppsFlyerAdRevenueAdType : adType,
            kAppsFlyerAdRevenueECPMPayload : "encrypt"
        ]
        
        let mediationNetwork = mediation == "max" ? MediationNetworkType.applovinMax : MediationNetworkType.googleAdMob
        
        AppsFlyerAdRevenue.shared().logAdRevenue(
            monetizationNetwork: adSourceName ?? "Unknown",
            mediationNetwork: mediationNetwork,
            eventRevenue: revenueValue,
            revenueCurrency: currencyCode,
            additionalParameters: adRevenueParams
        )
    }
    
}
