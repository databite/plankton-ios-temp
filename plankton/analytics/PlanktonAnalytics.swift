//
//  PlanktonAnalytics.swift
//  plankton
//
//  Created by Funtory on 3/21/1401 AP.
//


public class PlanktonAnalytics: Injectable {
    
    @Inject private var runtimeInfoManager: RuntimeInfoManager

    @Inject private var firebaseAnalytics: FirebaseAnalytics
    @Inject private var appsFlyerAnalytics: AppsFlyerAnalytics
    @Inject private var appMetricaAnalytics: AppMetricaAnalytics

    private var defaultParams: DefaultParams? = nil

    private var providers: [String: AnalyticsProvider] = [:]
    
    init(){
        LogHelper.d("Initializing PlanktonAnalytics")
        providers[firebaseAnalytics.plistKey.lowercased()] = firebaseAnalytics
        addIfDependencyExists(appsFlyerAnalytics)
        addIfDependencyExists(appMetricaAnalytics)
    }
    
    private func addIfDependencyExists(_ provider: AnalyticsProvider){
        do {
            let configKey = try Utils.readPlistElement(provider.plistKey)
            provider.initialize(configKey, runtimeInfoManager.appId, completion: {
                self.providers[provider.plistKey.lowercased()] = provider
                LogHelper.d("Added \(Utils.getClassName(provider)) AnalyticsProvider successfully")
            })
            
        } catch {
            LogHelper.e("\(Utils.getClassName(provider)) AnalyticsProvider configs does not exist!")
        }
    }

    public func setUserProperty(_ key: String, _ value: String){
        LogHelper.d("SetUserProperty called. key: \(key), value: \(value)")
        providers.values.forEach{ p in
            p.setUserProperty(key, value)
        }
    }

    public func logEvent(_ provider: String, _ eventName: String, _ paramsJson: String, _ isInternal: Bool = false){
        let hasDefaults = setDefaultParams(paramsJson)
        let appendDefaults = hasDefaults || isInternal
        let providerNames = provider.splitAndTrim(",")
        providerNames.forEach{ name in
            providers[name]?.logEvent(eventName, getParams(paramsJson, appendDefaults))
        }
    }
    
    private func setDefaultParams(_ paramsJson: String) -> Bool {
        var hasDefaults = false
        do {
            let defaults = try JSONDecoder().decode(DefaultParams.self, from: Data(paramsJson.utf8))
            if defaults.sessionNumber != nil {
                defaultParams = defaults
                hasDefaults = true
            }
        } catch {
            LogHelper.d("An error occured during parsing of parameters json")
        }
        return hasDefaults
    }
    
    private func getParams(_ paramsJson: String, _ appendDefaults: Bool) -> [String: Any] {
        let params = try! Utils.jsonToDict(paramsJson)
        if appendDefaults && defaultParams != nil {
            
            let defaults = try! Utils.jsonToDict(String(decoding: JSONEncoder().encode(defaultParams), as: UTF8.self))
            return params + defaults
        } else {
            return params
        }
    }

    public func adsAvailable(_ adType: String, _ placement: String, _ isLoaded: Bool) {
        providers.values.forEach { p in
            p.adsAvailable(adType, placement, isLoaded)
        }
    }

    public func adsStarted(_ adType: String, _ placement: String, _ isStarted: Bool) {
        providers.values.forEach { p in
            p.adsStarted(adType, placement, isStarted)
        }
    }

    public func adsWatch(_ adType: String, _ placement: String, _ result: AdWatchState) {
        providers.values.forEach { p in
            p.adsWatch(adType, placement, result)
        }
    }
}
