//
// Created by Funtory on 27/06/2022.
//

import FirebaseRemoteConfig

public class PlanktonRemoteConfig: Injectable {

    private lazy var remoteConfig: RemoteConfig = {
        RemoteConfig.remoteConfig()
    }()

    public func setRemoteConfigMinimumFetchInterval(_ interval: Int64){
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = TimeInterval(interval)
        remoteConfig.configSettings = settings
    }

    public func fetchRemoteConfig(){
        remoteConfig.fetchAndActivate{ (status, error) -> Void in
            if status == RemoteConfigFetchAndActivateStatus.error {
                UnityMessaging.remoteConfig.OnRemoteConfigFetchFailed()
            } else {
                var allKeys: [String] = []
                allKeys.append(contentsOf: self.remoteConfig.allKeys(from: .static))
                allKeys.append(contentsOf: self.remoteConfig.allKeys(from: .remote))
                UnityMessaging.remoteConfig.onRemoteConfigFetched(allKeys.joined(separator: ","))
            }
        }
    }
    
    public func getRemoteConfigValue(_ key: String, _ defaultValue: String) -> String {
        let value = remoteConfig.configValue(forKey: key).stringValue
        if value?.isEmpty == true {
            return defaultValue
        } else {
            return value!
        }
    }
}
