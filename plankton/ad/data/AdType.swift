//
//  AdType.swift
//  MyFramework
//
//  Created by Funtory on 3/16/1401 AP.
//


enum AdType: String {
    case BANNER = "BANNER"
    case INTERSTITIAL = "INTERSTITIAL"
    case REWARDED = "REWARDED"
    case UNKNOWN = "UNKNOWN"
    
    init?(ignoreCase rawValue: String) {
        self.init(rawValue: rawValue.uppercased())
    }

    func toLowerString() -> String {
        self.rawValue.lowercased()
    }
}
