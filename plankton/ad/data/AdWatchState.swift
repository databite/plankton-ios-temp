//
// Created by Funtory on 13/08/2022.
//

import Foundation

public enum AdWatchState: String {
    case Watched = "watched"
    case Clicked = "clicked"
    case Canceled = "canceled"
    case Nagayidam = "nagayidam"
}
