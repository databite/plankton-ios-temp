//
// Created by Funtory on 05/12/2022.
//

import Foundation

class InternalError {
    static let NOT_LOADED_CODE = 1234
    static let NOT_LOADED_MESSAGE = "Ad has not loaded yet."
}
