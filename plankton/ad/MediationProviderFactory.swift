//
// Created by Funtory on 09/07/2022.
//


class MediationProviderFactory {
    private static let PROVIDER_PLIST_KEY = "adMediationProvider"
    
    private static let AD_MOB = "admob"
    private static let APP_LOVIN = "applovin"

    static func retrieveMediationProvider() -> PlanktonMediationProvider {
        do {
            let providerName = try Utils.readPlistElement(PROVIDER_PLIST_KEY)
            
            switch providerName {
            case AD_MOB:
                return AdMobMediationProvider()
            case APP_LOVIN:
                return AppLovinMediationProvider()
            default:
                throw NSError()
            }
            
        } catch {
            return AdMobMediationProvider()
        }
    }
}
