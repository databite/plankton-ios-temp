//
//  PlanktonAd.swift
//  plankton
//
//  Created by Funtory on 3/21/1401 AP.
//


public class PlanktonAd : Injectable {

    @Inject
    var bannerCycleManager: BannerCycleManager

    @Inject
    var interstitialCycleManager: InterstitialCycleManager

    @Inject
    var rewardedCycleManager: RewardedCycleManager

    private var mediationProvider: PlanktonMediationProvider

    private var cycleManagers: [AdType: PlanktonCycleManager] = [:]

    private var isStarted = false

    internal init() {
        mediationProvider = MediationProviderFactory.retrieveMediationProvider()
    }

    public func initialize() {
        LogHelper.d("Initializing Ad")
        mediationProvider.initialize()
        if !isStarted {
            startCycleManager(mediationProvider.bannerAd, bannerCycleManager)
            startCycleManager(mediationProvider.interstitialAd, interstitialCycleManager)
            startCycleManager(mediationProvider.rewardedAd, rewardedCycleManager)
            isStarted = true
        }
    }

    private func startCycleManager(_ ad: PlanktonAdProvider, _ cycleManager: PlanktonCycleManager) {
        cycleManagers[ad.getAdType()] = cycleManager
        cycleManager.start(ad: ad)
    }

    public func isReady(type: String) -> Bool {
        cycleManagers[AdType(ignoreCase: type)!]!.isReady()
    }

    public func isOnline() -> Bool {
        let onlineState = cycleManagers[AdType.INTERSTITIAL]!.isOnline() && cycleManagers[AdType.REWARDED]!.isOnline()
        LogHelper.d("IsOnline: \(onlineState)")
        return onlineState
    }

    public func show(type: String, placement: String) {
        cycleManagers[AdType(ignoreCase: type)!]!.show(placement)

    }

    public func hide(type: String) {
        cycleManagers[AdType(ignoreCase: type)!]!.hide()
    }

//    public func showBanner() {
//        mediationProvider.showBanner()
//    }
//
//    public func hideBanner() {
//        mediationProvider.hideBanner()
//    }
//
//    public func loadInterstitial() {
//        mediationProvider.loadInterstitial()
//    }
//
//    public func showInterstitial(_ placement: String) {
//        mediationProvider.showInterstitial(placement)
//    }
//
//    public func loadRewarded() {
//        mediationProvider.loadRewarded()
//    }
//
//    public func showRewarded(_ placement: String) {
//        mediationProvider.showRewarded(placement)
//    }

    public func showMediationTestSuite() {
        mediationProvider.showMediationTestSuite()
    }
}
