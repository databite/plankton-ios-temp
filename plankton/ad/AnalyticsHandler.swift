//
// Created by Funtory on 04/12/2022.
//

import Foundation

class AnalyticsHandler: Injectable {

    private let EVENT_AD_LOAD_RESULT = "ad_load_result"
    private let EVENT_AD_SHOW_RESULT = "ad_show_result"

    private let PLANKTON_REVENUE_EVENT = "plankton_revenue"

    private let PARAM_INTERNET = "internet"

    private let PARAM_AD_FORMAT = "ad_format"
    private let PARAM_SUCCESS = "success"
    private let PARAM_PLACEMENT = "placement"
    private let PARAM_AD_SOURCE = "ad_source"
    private let PARAM_RESPONSE_ID = "response_id"
    private let PARAM_CODE = "error_code"
    private let PARAM_MESSAGE = "error_message"

    private let PROVIDER_FIREBASE = "firebase"
    private let PROVIDER_YANDEX = "yandex"

    private let UNKNOWN = "unknown"

    private let FIREBASE_PARAM_VALUE_MAX_CHARS = 100

    private let EVENT_ADS_AVAILABLE = "video_ads_available"
    private let EVENT_ADS_STARTED = "video_ads_started"
    private let EVENT_ADS_WATCH = "video_ads_watch"

    private let PARAM_AD_TYPE = "ad_type"
    private let PARAM_RESULT = "result"
    private let PARAM_CONNECTION = "connection"

    @Inject
    var planktonAnalytics: PlanktonAnalytics

    @Inject
    var runtimeInfoManager: RuntimeInfoManager

    func adLoadResult(_ adType: AdType, success: Bool, code: Int? = nil, message: String? = nil) {
        var params: [String: Any] = [
            PARAM_AD_FORMAT: adType.toLowerString(),
            PARAM_SUCCESS: success,
            PARAM_INTERNET: runtimeInfoManager.internetState.rawValue
        ]
        if !success {
            params[PARAM_CODE] = code ?? UNKNOWN
            params[PARAM_MESSAGE] = getParamValueSubstring(message) ?? UNKNOWN
        }

        planktonAnalytics.logEvent(PROVIDER_FIREBASE, EVENT_AD_LOAD_RESULT, params.toJsonString(), true)
    }

    func adShowResult(_ adType: AdType, success: Bool, placement: String, network: String? = nil, responseId: String? = nil, code: Int? = nil, message: String? = nil) {
        var params: [String: Any] = [
            PARAM_AD_FORMAT: adType.toLowerString(),
            PARAM_SUCCESS: success,
            PARAM_PLACEMENT: placement,
            PARAM_INTERNET: runtimeInfoManager.internetState.rawValue
        ]
        if !success {
            params[PARAM_CODE] = code ?? UNKNOWN
            params[PARAM_MESSAGE] = getParamValueSubstring(message) ?? UNKNOWN
        } else {
            params[PARAM_AD_SOURCE] = network ?? UNKNOWN
            params[PARAM_RESPONSE_ID] = responseId ?? UNKNOWN
        }

        planktonAnalytics.logEvent(PROVIDER_FIREBASE, EVENT_AD_SHOW_RESULT, params.toJsonString(), true)
    }

    private func getParamValueSubstring(_ value: String?) -> String? {
        value?.substring(to: min(value!.count, FIREBASE_PARAM_VALUE_MAX_CHARS))
    }

    func appMetricaAdsAvailable(_ adType: AdType, _ placement: String, _ isLoaded: Bool) {
        let result = isLoaded ? "success" : "not_available"
        let params = getAppMetricaEventParams(adType, placement, result)
        planktonAnalytics.logEvent(PROVIDER_YANDEX, EVENT_ADS_AVAILABLE, params.toJsonString(), false)
    }

    func appMetricaAdsStarted(_ adType: AdType, _ placement: String, _ isStarted: Bool) {
        let result = isStarted ? "start" : "fail"
        let params = getAppMetricaEventParams(adType, placement, result)
        planktonAnalytics.logEvent(PROVIDER_YANDEX, EVENT_ADS_STARTED, params.toJsonString(), false)
    }

    func appMetricaAdsWatch(_ adType: AdType, _ placement: String, _ result: AdWatchState) {
        let params = getAppMetricaEventParams(adType, placement, result.rawValue)
        planktonAnalytics.logEvent(PROVIDER_YANDEX, EVENT_ADS_WATCH, params.toJsonString(), false)
    }

    private func getAppMetricaEventParams(_ adType: AdType, _ placement: String, _ result: String) -> [String: Any] {
        [
            PARAM_AD_TYPE: adType.toLowerString(),
            PARAM_PLACEMENT: placement,
            PARAM_RESULT: result,
            PARAM_CONNECTION: runtimeInfoManager.internetState.toBoolean()
        ]
    }
}
