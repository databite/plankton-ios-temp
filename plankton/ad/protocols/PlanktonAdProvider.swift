//
// Created by Funtory on 04/12/2022.
//

import Foundation

protocol PlanktonAdProvider {

    var listener: PlanktonAdListener? { get set }

    func hide()

    func load()

    func show()

    func getAdType() -> AdType

    func isOnline() -> Bool
}
