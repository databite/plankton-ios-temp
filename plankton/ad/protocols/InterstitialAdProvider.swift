//
// Created by Funtory on 09/07/2022.
//


protocol PlanktonInterstitialAd: PlanktonAdProvider {}

extension PlanktonInterstitialAd {
    func hide() {}

    func getAdType() -> AdType {
        AdType.INTERSTITIAL
    }
}
