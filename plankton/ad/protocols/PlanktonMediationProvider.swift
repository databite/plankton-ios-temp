//
// Created by Funtory on 09/07/2022.
//


protocol PlanktonMediationProvider {

    var bannerAd: PlanktonBannerAd { get set }

    var interstitialAd: PlanktonInterstitialAd { get set }

    var rewardedAd: PlanktonRewardedAd { get set }
    
    func initialize()

    func showMediationTestSuite()
}
