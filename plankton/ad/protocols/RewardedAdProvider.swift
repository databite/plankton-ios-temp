//
// Created by Funtory on 09/07/2022.
//


protocol PlanktonRewardedAd: PlanktonAdProvider {}

extension PlanktonRewardedAd {
    func hide() {}

    func getAdType() -> AdType {
        AdType.REWARDED
    }
}
