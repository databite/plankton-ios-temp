//
// Created by Funtory on 09/07/2022.
//


protocol PlanktonBannerAd: PlanktonAdProvider {
    
    var rootView: UIViewController { get }
}

extension PlanktonBannerAd {
    func show() {}

    func getAdType() -> AdType {
        AdType.BANNER
    }

    func calculateBannerXPosition(_ width: CGFloat)  -> CGFloat {
        (rootView.view.frame.width - width) / 2
    }

    func calculateBannerYPosition(_ height: CGFloat)  -> CGFloat{
        rootView.view.frame.height - height
    }
    
    func topMostController() -> UIViewController {
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        return topController
    }
    
}
