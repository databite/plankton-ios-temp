//
// Created by Funtory on 09/07/2022.
//

import AppLovinSDK

class AppLovinMediationProvider: PlanktonMediationProvider {
    
    var bannerAd: PlanktonBannerAd = AppLovinBanner()

    var interstitialAd: PlanktonInterstitialAd = AppLovinInterstitial()

    var rewardedAd: PlanktonRewardedAd = AppLovinRewarded()
    
    func initialize() {
        let settings = ALSdkSettings()
        settings.consentFlowSettings.isEnabled = true
        settings.consentFlowSettings.privacyPolicyURL = URL(string: "https://aigames.ae/policy")

        // Terms of Service URL is optional
        settings.consentFlowSettings.termsOfServiceURL = URL(string: "https://aigames.ae/policy")

        let appLovinSdk = ALSdk.shared(with: settings)!
        appLovinSdk.mediationProvider = "max"
        
//        appLovinSdk.settings.isVerboseLogging = true
                                
        appLovinSdk.initializeSdk { (configuration: ALSdkConfiguration) in
            LogHelper.d("AppLovin MAX Initialized with configuration: \(configuration)")
        }
    }
    
//    func showBanner() {
//        bannerAd.show()
//    }
//    
//    func hideBanner() {
//        bannerAd.hide()
//    }
//    
//    func loadInterstitial() {
//        interstitialAd.load()
//    }
//    
//    func showInterstitial(_ placement: String) {
//        interstitialAd.show(placement)
//    }
//    
//    func loadRewarded() {
//        rewardedAd.load()
//    }
//    
//    func showRewarded(_ placement: String) {
//        rewardedAd.show(placement)
//    }
    
    func showMediationTestSuite() {
    }
}
