//
// Created by Funtory on 09/07/2022.
//

import AppLovinSDK

class AppLovinRewarded: NSObject, MARewardedAdDelegate, MAAdRevenueDelegate, PlanktonRewardedAd, AppLovinAdProvider {
    
    @Inject public var appsFlyerAnalytics: AppsFlyerAnalytics

    private let ZONE_ID_KEY = "rewardedZoneId"

    private var ad: MARewardedAd!
    
    var listener: PlanktonAdListener? = nil

    func load() {
        let zoneId = try! Utils.readPlistElement(ZONE_ID_KEY)

        ad = MARewardedAd.shared(withAdUnitIdentifier: zoneId)

//        resetWatchState()
        ad.delegate = self
        ad.revenueDelegate = self
        ad.load()
    }
    
    func didPayRevenue(for ad: MAAd) {
        appsFlyerAnalytics.logRevenue(
            adType: ad.format.label,
            adUnitId: ad.adUnitIdentifier,
            adSourceName: ad.networkName,
            revenueValue: NSDecimalNumber(value: ad.revenue),
            currencyCode: "USD",
            mediation: "max"
        )
    }

    func show() {
        ad.show()
    }

    func didLoad(_ adObject: MAAd) {
        listener?.onAdLoaded()
        adLoaded()
    }

    func didFailToLoadAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError) {
        listener?.onAdFailedToLoad(code: error.code.rawValue, message: error.message)
        adFailedToLoad(error.code.rawValue, error.message)
    }

    func didDisplay(_ adObject: MAAd) {
        listener?.onAdShowed(adNetwork: adObject.networkName)
    }

    func didFail(toDisplay adObject: MAAd, withError error: MAError) {
        listener?.onAdFailedToShow(code: error.code.rawValue, message: error.message)
    }

    func didHide(_ adObject: MAAd) {
        listener?.onAdClosed()
    }

    func didClick(_ adObject: MAAd) {
        listener?.onAdClicked()
    }

    // MARK: MARewardedAdDelegate Protocol

    func didRewardUser(for adObject: MAAd, with reward: MAReward) {
        listener?.onRewardEarned()
    }

    func didStartRewardedVideo(for adObject: MAAd) {
    }

    func didCompleteRewardedVideo(for adObject: MAAd) {
    }
}
