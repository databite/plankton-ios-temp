//
// Created by Funtory on 07/12/2022.
//

import Foundation
import AppLovinSDK

protocol AppLovinAdProvider : PlanktonAdProvider {}

extension AppLovinAdProvider {

    func isOnline() -> Bool {
        AppLovinHolder.isOnline
    }

    func adLoaded() {
        AppLovinHolder.isOnline = true
    }

    func adFailedToLoad(_ code: Int, _ message: String) {
        AppLovinHolder.isOnline = checkNetworkState(code, message)
    }

    func checkNetworkState(_ code: Int, _ message: String) -> Bool {
        let keywords = ["connect", "internet", "dns", "timed out", "timeout", "host"]
        let networkErrorCodes = [MAErrorCode.networkError.rawValue, MAErrorCode.noNetwork.rawValue, MAErrorCode.noFill.rawValue, -102, -103]

        return !(networkErrorCodes.contains(code) || keywords.contains(where: message.contains))
    }
}

struct AppLovinHolder {
    static var isOnline = true
}
