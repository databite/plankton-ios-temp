//
// Created by Funtory on 09/07/2022.
//

import AppLovinSDK
import CoreGraphics
import UIKit

class AppLovinBanner: NSObject, MAAdViewAdDelegate, MAAdRevenueDelegate, PlanktonBannerAd, AppLovinAdProvider {
    
    @Inject public var appsFlyerAnalytics: AppsFlyerAnalytics

    private let ZONE_ID_KEY = "bannerZoneId"

    private var adView: MAAdView!
    private var backView: UIView!

    var listener: PlanktonAdListener? = nil

    lazy var rootView = topMostController()

    private let IPAD_HEIGTH = CGFloat(90)
    private let IPHONE_HEIGHT = CGFloat(50)

    func load() {
        let zoneId = try! Utils.readPlistElement(ZONE_ID_KEY)
        adView = MAAdView(adUnitIdentifier: zoneId)
        adView.delegate = self
        adView.revenueDelegate = self
        
        let height: CGFloat = MAAdFormat.banner.adaptiveSize.height
            
        // Stretch to the width of the screen for banners to be fully functional
        let width: CGFloat = UIScreen.main.bounds.width
    
        adView.frame = CGRect(x: calculateBannerXPosition(width), y: calculateBannerYPosition(height), width: width, height: height)
//        showBackView(width, height)
//        bannerView.backgroundColor = UIColor.black
        adView.setExtraParameterForKey("adaptive_banner", value: "true")
        
        rootView.viewIfLoaded?.addSubview(adView)
        adView.isHidden = false
        
        adView.loadAd()
    }
    
    private func showBackView(_ width: CGFloat, _ height: CGFloat){
        backView = UIView(frame: CGRect(x: calculateBannerXPosition(width), y: calculateBannerYPosition(height), width: width, height: height))
        backView.backgroundColor = UIColor.black
        backView.isHidden = false
        rootView.viewIfLoaded?.addSubview(backView)
    }
    
    func hide() {
        if adView == nil { return }
        
//        backView.isHidden = true
        adView.isHidden = true
        stopAutoRefresh()
        adView = nil
    }

    private func stopAutoRefresh(){
        adView.setExtraParameterForKey("allow_pause_auto_refresh_immediately", value: "true")
        adView.stopAutoRefresh()
    }
    
    func didPayRevenue(for ad: MAAd) {
        appsFlyerAnalytics.logRevenue(
            adType: ad.format.label,
            adUnitId: ad.adUnitIdentifier,
            adSourceName: ad.networkName,
            revenueValue: NSDecimalNumber(value: ad.revenue),
            currencyCode: "USD",
            mediation: "max"
        )
    }

    func didLoad(_ adObject: MAAd) {
        listener?.onAdLoaded()
        listener?.onAdShowed(adNetwork: adObject.networkName)
    }

    func didFailToLoadAd(forAdUnitIdentifier adUnitIdentifier: String, withError error: MAError) {
        listener?.onAdFailedToLoad(code: error.code.rawValue, message: error.message)
    }

    func didFail(toDisplay adObject: MAAd, withError error: MAError) {
        listener?.onAdFailedToShow(code: error.code.rawValue, message: error.message)
    }

    func didClick(_ adObject: MAAd) {
        listener?.onAdClicked()
    }

    func didExpand(_ adObject: MAAd) {
        LogHelper.d("Banner view didExpand")
    }

    func didCollapse(_ adObject: MAAd) {
        LogHelper.d("Banner view didCollapse")
    }

    // MARK: Deprecated Callbacks (MAX won't call these methods)
    
    func didDisplay(_ adObject: MAAd) {
        LogHelper.d("Banner view didDisplay")
    }
    
    func didHide(_ adObject: MAAd) {
        LogHelper.d("Banner view didHide")
    }
}
