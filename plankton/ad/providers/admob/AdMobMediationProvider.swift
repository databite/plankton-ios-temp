//
// Created by Funtory on 09/07/2022.
//

import GoogleMobileAds

class AdMobMediationProvider: PlanktonMediationProvider {
    
    var bannerAd: PlanktonBannerAd = AdMobBannerAd()

    var interstitialAd: PlanktonInterstitialAd = AdMobInterstitialAd()

    var rewardedAd: PlanktonRewardedAd = AdMobRewardedAd()
    
    func initialize() {
        GADMobileAds.sharedInstance().start {status in
            let adapterStatuses = status.adapterStatusesByClassName
            LogHelper.d("AdMob Initialized.")
            for adapter in adapterStatuses {
                let adapterStatus = adapter.value
                LogHelper.d("Adapter Name: \(adapter.key), State: \(GADAdapterInitializationState.RawValue(adapterStatus.state.rawValue))")
            }
        }
    }
    
//    func showBanner() {
//        bannerAd.show()
//    }
//
//    func hideBanner() {
//        bannerAd.hide()
//    }
//
//    func loadInterstitial() {
//        interstitialAd.load()
//    }
//
//    func showInterstitial(_ placement: String) {
//        interstitialAd.show(placement)
//    }
//
//    func loadRewarded() {
//        rewardedAd.load()
//    }
//
//    func showRewarded(_ placement: String) {
//        rewardedAd.show(placement)
//    }
    
    func showMediationTestSuite() {
        
    }
    
}
