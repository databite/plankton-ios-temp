//
//  InterstitialAd.swift
//  MyFramework
//
//  Created by Funtory on 3/8/1401 AP.
//

import GoogleMobileAds

class AdMobInterstitialAd: NSObject, GADFullScreenContentDelegate, PlanktonInterstitialAd, AdMobAdProvider {
    
    @Inject public var appsFlyerAnalytics: AppsFlyerAnalytics

    private let ZONE_ID_KEY = "interstitialZoneId"

    private var ad: GADInterstitialAd? = nil

    var listener: PlanktonAdListener? = nil

    func load() {
        let zoneId = try! Utils.readPlistElement(ZONE_ID_KEY)
        let request = GADRequest()

        GADInterstitialAd.load(withAdUnitID:zoneId, request: request, completionHandler: { [self] adObject, error in
            if let error = error {
                listener?.onAdFailedToLoad(code: error._code, message: error.localizedDescription)
                adFailedToLoad(error._code, error.localizedDescription)
                ad = nil
                return
            }
            listener?.onAdLoaded()
            adLoaded()
            ad = adObject
            ad?.fullScreenContentDelegate = self
            ad?.paidEventHandler = { (adValue: GADAdValue) in
                // Extract the impression-level ad revenue data.
                let revenueValue = adValue.value
                let currencyCode = adValue.currencyCode

                let responseInfo = self.ad!.responseInfo
                let loadedAdNetworkResponseInfo = responseInfo.loadedAdNetworkResponseInfo
                let adSourceName = loadedAdNetworkResponseInfo?.adSourceName

                self.appsFlyerAnalytics.logRevenue(adType: "Interstitial", adUnitId: zoneId, adSourceName: adSourceName, revenueValue: revenueValue, currencyCode: currencyCode)
            }
        })
    }
    
    func show() {
        let root = UIApplication.shared.windows.first?.rootViewController
        ad?.present(fromRootViewController: root!)
    }

    func adWillPresentFullScreenContent(_ adObject: GADFullScreenPresentingAd) {
        let network = ad?.responseInfo.adNetworkClassName
        let responseId = ad?.responseInfo.responseIdentifier
        listener?.onAdShowed(adNetwork: network, responseId: responseId)
    }

    func ad(_ adObject: GADFullScreenPresentingAd, didFailToPresentFullScreenContentWithError error: Error){
        listener?.onAdFailedToShow(code: error._code, message: error.localizedDescription)
        ad = nil
    }

    func adDidDismissFullScreenContent(_ adObject: GADFullScreenPresentingAd) {
        listener?.onAdClosed()
        ad = nil
    }
}
