//
// Created by Funtory on 07/12/2022.
//

import Foundation
import GoogleMobileAds

protocol AdMobAdProvider : PlanktonAdProvider {}

extension AdMobAdProvider {
        
    func isOnline() -> Bool {
        AdMobHolder.isOnline
    }

    func adLoaded() {
        AdMobHolder.isOnline = true
    }

    func adFailedToLoad(_ code: Int, _ message: String) {
        AdMobHolder.isOnline = checkNetworkState(code)
    }

    func checkNetworkState(_ code: Int) -> Bool {
        let networkErrorCodes = [GADErrorCode.internalError.rawValue, GADErrorCode.networkError.rawValue]
        return !networkErrorCodes.contains(code)
    }
}

struct AdMobHolder {
    static var isOnline = true
}
