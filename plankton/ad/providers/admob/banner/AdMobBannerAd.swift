//
//  AdMobBannerAd.swift
//  MyFramework
//
//  Created by Funtory on 3/8/1401 AP.
//

import GoogleMobileAds

class AdMobBannerAd: NSObject, GADBannerViewDelegate, PlanktonBannerAd, AdMobAdProvider {
    
    @Inject public var appsFlyerAnalytics: AppsFlyerAnalytics

    private let ZONE_ID_KEY = "bannerZoneId"

    private var adView = GADBannerView()

    var listener: PlanktonAdListener? = nil

    lazy var rootView = topMostController()

    func load() {
        if UIDevice.current.userInterfaceIdiom == .phone {
            // iPhone
            createBannerView(GADAdSizeBanner)
            LogHelper.d("banner IPhone")
        } else {
            // iPad
            createBannerView(GADAdSizeFullBanner)
            LogHelper.d("bannerIPod")
        }
        
        let zoneId = try! Utils.readPlistElement(ZONE_ID_KEY)

        adView.adUnitID = zoneId
        adView.rootViewController = rootView
        adView.delegate = self
        adView.paidEventHandler = { (adValue: GADAdValue) in
            // Extract the impression-level ad revenue data.
            let revenueValue = adValue.value
            let currencyCode = adValue.currencyCode
            
            let responseInfo = self.adView.responseInfo!
            let loadedAdNetworkResponseInfo = responseInfo.loadedAdNetworkResponseInfo
            let adSourceName = loadedAdNetworkResponseInfo?.adSourceName
            
            self.appsFlyerAnalytics.logRevenue(adType: "Banner", adUnitId: zoneId, adSourceName: adSourceName, revenueValue: revenueValue, currencyCode: currencyCode)
        }
        rootView.viewIfLoaded?.addSubview(adView)
        adView.isHidden = false

        adView.load(GADRequest())
    }

    private func createBannerView(_ adSize: GADAdSize){
        adView.adSize = adSize
        let width = adSize.size.width
        let height = adSize.size.height
        adView.frame = CGRect(x: calculateBannerXPosition(width), y: calculateBannerYPosition(height), width: width, height: height)
    }

    func hide(){
        adView.isHidden = true
    }

    func bannerViewDidReceiveAd(_ bannerView: GADBannerView) {
        listener?.onAdLoaded()
    }

    // didFailToReceiveAdWithError
    func bannerView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: Error) {
        listener?.onAdFailedToLoad(code: error._code, message: error.localizedDescription)
    }

    func bannerViewDidRecordImpression(_ bannerView: GADBannerView) {
        let network = bannerView.responseInfo?.adNetworkClassName
        let responseId = bannerView.responseInfo?.responseIdentifier
        listener?.onAdShowed(adNetwork: network, responseId: responseId)
    }

    func bannerViewDidDismissScreen(_ bannerView: GADBannerView) {
        listener?.onAdClosed()
    }

    func bannerViewWillPresentScreen(_ bannerView: GADBannerView) {
        LogHelper.d("bannerViewWillPresentScreen")
    }
}
