//
// Created by Funtory on 04/12/2022.
//

import Foundation

class PlanktonCycleManager : Injectable, PlanktonAdListener {

    @Inject
    var analyticsHandler: AnalyticsHandler

    var delays = [0, 1, 2, 5, 10]
    var watchState = AdWatchState.Nagayidam
    
    var ad: PlanktonAdProvider? = nil
    var adType: AdType? = nil
    var loadFailCount = 0

    private var isAdReady = false
    private var placement: String = ""

    public func isReady() -> Bool {
        LogHelper.d("IsReady \(String(describing: adType?.rawValue)): \(isAdReady)")
        return isAdReady
    }
    
    public func isOnline() -> Bool {
        ad?.isOnline() ?? true
    }
    
    func load() {
        let delay = getDelay(loadFailCount, delays)
        LogHelper.d("Will load \(String(describing: adType?.rawValue)) in \(delay) seconds")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            LogHelper.d("Start loading \(String(describing: self.adType?.rawValue))...")
            self.ad?.load()
        }
    }

    public func start(ad: PlanktonAdProvider) {
        self.adType = ad.getAdType()
        self.ad = ad
        self.ad?.listener = self
        LogHelper.d("Started PlanktonCycleManager for \(String(describing: adType?.rawValue))")
        load()
    }
    
    public func show(_ placement: String) {
        self.placement = placement
        if isAdReady {
            LogHelper.d("Showing \(String(describing: adType?.rawValue)) with placement \(placement)")
            analyticsHandler.appMetricaAdsAvailable(adType!, placement, true)
            ad?.show()
        } else {
            analyticsHandler.appMetricaAdsAvailable(adType!, placement, false)
            onAdFailedToShow(code: InternalError.NOT_LOADED_CODE, message: InternalError.NOT_LOADED_MESSAGE)
        }
    }
    
    public func hide() {}
    
    private func getDelay(_ loadFailCount: Int, _ delays: [Int]) -> Double {
        Double(delays[min(loadFailCount, delays.count - 1)])
    }

    private func dropPrefix(_ text: String) -> String {
        let prefix = "GADMAdapter"
        if text.hasPrefix(prefix){
            return String(text.dropFirst(prefix.count))
        } else {
            return text
        }
    }

    func onAdLoaded() {
        LogHelper.d("OnAdLoaded \(String(describing: adType?.rawValue))")
        analyticsHandler.adLoadResult(adType!, success: true)
        isAdReady = true
        loadFailCount = 0
    }

    func onAdFailedToLoad(code: Int, message: String) {
        LogHelper.d("onAdFailedToLoad \(String(describing: adType?.rawValue)). code: \(code), message: \(message)")
        analyticsHandler.adLoadResult(adType!, success: false, code: code, message: message)
        isAdReady = false
        loadFailCount += 1
        load()
    }

    func onAdShowed(adNetwork: String?, responseId: String?) {
        let network = adNetwork ?? "Unknown"
        LogHelper.d("onAdShowed \(String(describing: adType?.rawValue) ). adNetwork: \(String(describing: dropPrefix(network))), responseId: \(String(describing: responseId))")
        analyticsHandler.adShowResult(adType!, success: true, placement: placement, network: dropPrefix(network), responseId: responseId)
        analyticsHandler.appMetricaAdsStarted(adType!, placement, true)
        isAdReady = false
    }

    func onAdFailedToShow(code: Int, message: String) {
        LogHelper.d("onAdFailedToShow \(String(describing: adType?.rawValue)). code: \(code), message: \(message)")
        UnityMessaging.ad.onAdClosed(false, false)
        analyticsHandler.adShowResult(adType!, success: false, placement: placement, code: code, message: message)
        analyticsHandler.appMetricaAdsStarted(adType!, placement, false)
    }

    func onAdClicked() {
        LogHelper.d("onAdClicked \(String(describing: adType?.rawValue))")
        watchState = AdWatchState.Clicked
    }

    func onAdClosed() {
        LogHelper.d("onAdClosed \(String(describing: adType?.rawValue))")
        analyticsHandler.appMetricaAdsWatch(adType!, placement, watchState)
        load()
    }

    func onRewardEarned() {
        LogHelper.d("onRewardEarned \(String(describing: adType?.rawValue))")
        if (watchState != AdWatchState.Clicked) {
            watchState = AdWatchState.Watched
        }
    }
}
