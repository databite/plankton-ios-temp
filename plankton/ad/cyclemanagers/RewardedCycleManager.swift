//
// Created by Funtory on 04/12/2022.
//

import Foundation

class RewardedCycleManager : PlanktonCycleManager {

    private var isRewardEarned = false

    private let initialWatchState = AdWatchState.Canceled

    override func start(ad: PlanktonAdProvider) {
        watchState = initialWatchState
        super.start(ad: ad)
    }

    override func load() {
        watchState = initialWatchState
        super.load()
    }

    override func onAdClosed() {
        UnityMessaging.ad.onAdClosed(true, isRewardEarned)
        super.onAdClosed()
        isRewardEarned = false
    }

    override func onRewardEarned() {
        super.onRewardEarned()
        isRewardEarned = true
    }
}
