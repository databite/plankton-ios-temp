//
// Created by Funtory on 04/12/2022.
//

import Foundation

class InterstitialCycleManager : PlanktonCycleManager {

    private let initialWatchState = AdWatchState.Watched
    
    override func start(ad: PlanktonAdProvider) {
        watchState = initialWatchState
        super.start(ad: ad)
    }

    override func load() {
        watchState = initialWatchState
        super.load()
    }

    override func onAdClosed() {
        UnityMessaging.ad.onAdClosed(true, true)
        super.onAdClosed()
    }
}
