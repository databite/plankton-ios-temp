//
// Created by Funtory on 04/12/2022.
//

import Foundation

class BannerCycleManager : PlanktonCycleManager {

    private let PLACEMENT = "banner"

    override func start(ad: PlanktonAdProvider) {
        watchState = AdWatchState.Nagayidam
        delays = [0, 1, 2, 5]
        
        adType = ad.getAdType()
        self.ad = ad
        self.ad?.listener = self
        LogHelper.d("Started PlanktonCycleManager for \(String(describing: adType?.rawValue))")
    }

    override func show(_ placement: String) {
        load()
    }

    override func hide() {
        LogHelper.d("Hiding banner")
        ad?.hide()
    }
    
    private func dropPrefix(_ text: String) -> String {
        let prefix = "GADMAdapter"
        if text.hasPrefix(prefix){
            return String(text.dropFirst(prefix.count))
        } else {
            return text
        }
    }

    override func onAdLoaded() {
        LogHelper.d("OnAdLoaded \(String(describing: adType?.rawValue))")
        analyticsHandler.adLoadResult(adType!, success: true)
        loadFailCount = 0
    }

    override func onAdFailedToLoad(code: Int, message: String) {
        LogHelper.d("onAdFailedToLoad \(String(describing: adType?.rawValue)). code: \(code), message: \(message)")
        analyticsHandler.adLoadResult(adType!, success: false, code: code, message: message)
        loadFailCount += 1
        show(PLACEMENT)
    }

    override func onAdShowed(adNetwork: String?, responseId: String?) {
        let network = adNetwork ?? "Unknown"
        LogHelper.d("onAdShowed \(String(describing: adType?.rawValue)). adNetwork: \(dropPrefix(network)), responseId: \(String(describing: responseId))")
        analyticsHandler.adShowResult(adType!, success: true, placement: PLACEMENT, network: dropPrefix(network), responseId: responseId)
    }

    override func onAdFailedToShow(code: Int, message: String) {
        LogHelper.d("onAdFailedToShow \(String(describing: adType?.rawValue)). code: \(code), message: \(message)")
        analyticsHandler.adShowResult(adType!, success: false, placement: PLACEMENT, code: code, message: message)
        show(PLACEMENT)
    }

    override func onAdClosed() {}

    override func onAdClicked() {}

    override func onRewardEarned() {}
}
