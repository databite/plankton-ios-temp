//
// Created by Funtory on 04/12/2022.
//

import Foundation

protocol PlanktonAdListener {
    func onAdLoaded()

    func onAdFailedToLoad(code: Int, message: String)

    func onAdShowed(adNetwork: String?, responseId: String?)

    func onAdFailedToShow(code: Int, message: String)

    func onAdClicked()

    func onAdClosed()

    func onRewardEarned()
}

extension PlanktonAdListener {
    func onAdShowed(adNetwork: String?) {
        onAdShowed(adNetwork: adNetwork, responseId: nil)
    }
}
