//
//  Plankton.swift
//  MyFramework
//
//  Created by Funtory on 3/8/1401 AP.
//

import AppTrackingTransparency
import FirebaseCore
import FBAudienceNetwork

public class Plankton {
    
    private let TENJIN_PLIST_KEY = "tenjinKey"
    private let MANUAL_IDFA_KEY = "manualIdfa"

    public init(){}

    private let dependencyManager = DependencyManager()

    @Inject private var runtimeInfoManager: RuntimeInfoManager
    
    @Inject public var ad: PlanktonAd
    @Inject public var analytics: PlanktonAnalytics
    @Inject public var remoteConfig: PlanktonRemoteConfig
    @Inject public var rateApp: PlanktonRateApp
    @Inject public var privacy: PlanktonPrivacy

    public func initialize() {
        LogHelper.d("Initializing plankton...")
        privacy.initialize()
        if try! Utils.readPlistElement(MANUAL_IDFA_KEY).lowercased() == "true"{
            requestATT()
        }
        FirebaseApp.configure()
        ad.initialize()
    }

    private func requestATT(){
        LogHelper.d("Requesting IDFA...")
        if #available(iOS 14.5, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                switch status {
                case .authorized:
                    LogHelper.d("ATT Authorized")
                    self.connectTenjin()
                    FBAdSettings.setAdvertiserTrackingEnabled(true)
                case .denied:
                    LogHelper.d("ATT Denied")
                case .notDetermined:
                    LogHelper.d("ATT Not Determined")
                case .restricted:
                    LogHelper.d("ATT Restricted")
                @unknown default:
                    LogHelper.d("ATT Unknown")
                }
            })
        } else {
            LogHelper.d("ATT not required: iOS version less than 14")
            connectTenjin()
        }
    }
    
    private func connectTenjin(){
        do {
            let tenjinKey = try Utils.readPlistElement(TENJIN_PLIST_KEY)
            LogHelper.d("Connecting Tenjin...")
            TenjinSDK.getInstance(tenjinKey)
            TenjinSDK.connect()
            if isFirstLaunch(){
                LogHelper.d("Tenjin registering for ad network attribution")
                TenjinSDK.registerAppForAdNetworkAttribution()
            }
        } catch {
            LogHelper.d("Tenjin config doesn't exist!")
        }
        
    }

    private func isFirstLaunch() -> Bool {
        runtimeInfoManager.playerInfo.sessionNumber == 1
    }
}
