//
// Created by Funtory on 6/20/2022 AD.
//

class RuntimeInfoManager: Injectable {
    
    private let PLAYER_INFO_KEY = "PlayerInfo"
    private let SESSION_NUMBER_KEY = "SessionNumber"
    
    private let APP_ID_PLIST_KEY = "appId"
    
    private(set) var playerInfo: PlayerInfo!
    
    private(set) var internetState = InternetState.Unknown
    
    var appId = ""

    init(){
        LogHelper.d("Initializing RuntimeInfoManager ...")
        initPlayerInfo()
        appId = readAppId()
    }
    
    private func initPlayerInfo(){
        LogHelper.d("Initializing player info...")
        var pInfo = readPlayerInfo()
        
        if pInfo == nil {
            LogHelper.d("Player info doesn't exist. Creating a new one...")
            pInfo = PlayerInfo(startingTime: Int64(Date().timeIntervalSince1970))
        } else {
            LogHelper.d("Player info exists: \(pInfo!)")
            pInfo!.sessionNumber = pInfo!.sessionNumber + 1
            pInfo!.startingTime = Int64(Date().timeIntervalSince1970)
        }
        
        updatePlayerInfo(pInfo!)
    }
    
    private func readPlayerInfo() -> PlayerInfo? {
        return UserDefaultsHelper.getStruct(PLAYER_INFO_KEY, PlayerInfo.self)
    }
    
    func updatePlayerInfo(_ pInfo: PlayerInfo) {
        LogHelper.d("Updating Player info: \(pInfo)")
        playerInfo = pInfo
        writePlayerInfo(pInfo)
    }
    
    private func writePlayerInfo(_ pInfo: PlayerInfo) {
        UserDefaultsHelper.setStruct(PLAYER_INFO_KEY, data: pInfo)
    }

//    private func readSessionNumber() -> Int {
//        LogHelper.d("Reading session number")
//        let session = UserDefaultsHelper.getInt(SESSION_NUMBER_KEY) + 1
//        UserDefaultsHelper.setValue(SESSION_NUMBER_KEY, session)
//        LogHelper.d("Session Number: \(session)")
//        return session
//    }
    
    private func readAppId() -> String {
        let appId = try! Utils.readPlistElement(APP_ID_PLIST_KEY)
        LogHelper.d("App Id: \(appId)")
        return appId
    }
    
    func setInternetState(_ state: InternetState) {
        LogHelper.d("Internet state: \(state.rawValue)")
        if (internetState != state) {
            LogHelper.d("Internet state changed from \(internetState) to \(state)")
        }
        internetState = state
    }
}
