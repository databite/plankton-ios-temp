//
// Created by Funtory on 13/08/2022.
//

class NetworkManager: Injectable {
    
    @Inject
    var runtimeInfoManager: RuntimeInfoManager
    
    init() {
        checkConnection()
    }
    
    private let ConnectionCheckAddress = "https://connectivitycheck.gstatic.com/generate_204"

    private func checkConnection() {
        LogHelper.d("Checking connection")
        let delay = 10.0
                
        Timer.scheduledTimer(withTimeInterval: delay, repeats: true) { timer in
            LogHelper.d("Sending connection request...")
            self.executeConnectionTask(delay)
        }.fire()
    }
    
    private func executeConnectionTask(_ timeout: Double) {
        var request = URLRequest(url: URL(string: ConnectionCheckAddress)!)
        request.timeoutInterval = timeout
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            LogHelper.d("Response: \(String(describing: response))")
            if error != nil {
                self.runtimeInfoManager.setInternetState(InternetState.Offline)
            } else if (response as? HTTPURLResponse)?.statusCode == 204 {
                self.runtimeInfoManager.setInternetState(InternetState.Online)
            } 
        }
        
        task.resume()
    }
}
