//
// Created by Funtory on 09/07/2022.
//

import Foundation

struct PlayerInfo: Codable {
    
    var startingTime: Int64;

    var sessionNumber: Int = 1;

    var levelCount: Int = 0;

    var currentMode: String = "";

    var currentLevel: Int64 = 0;
}
