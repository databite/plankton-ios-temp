//
// Created by Funtory on 29/08/2022.
//


struct DefaultParams: Codable {

    var sessionNumber: Int?
    var sessionTime: Int?
    var currentLevel: Int?
    var currentMode: String?
    var currencies: String?
    var internet: String?

    enum CodingKeys: String, CodingKey {
        case sessionNumber = "session_number"
        case sessionTime = "session_time"
        case currentLevel = "current_level"
        case currentMode = "current_mode"
        case currencies = "currencies"
        case internet = "internet"
    }
}
