//
//  AdFailReason.swift
//  plankton
//
//  Created by Funtory on 03/08/2022.
//

enum AdFailReason: String {
    case AdNull = "ad_null"
    case NetworkFailure = "network_failure"
}
