//
// Created by Funtory on 13/08/2022.
//

import Foundation

enum InternetState: String {
    case Online = "Online"
    case Offline = "Offline"
    case Unknown = "Unknown"
    
    func toBoolean() -> Bool {
        return self == Self.Online
    }
}
