//
//  ConsentState.swift
//  plankton
//
//  Created by Funtory on 27/07/2022.
//

enum ConsentState: String {
    case Required = "Required"
    case NotRequired = "NotRequired"
    case Unknown = "Unknown"
}
