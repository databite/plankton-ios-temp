//
//  Utils.swift
//  plankton
//
//  Created by Funtory on 3/17/1401 AP.
//

import Foundation

class Utils {
    
    static func readPlistElement(_ key: String) throws -> String {
        guard let zoneId = Bundle.main.object(forInfoDictionaryKey: key) as? String
        else { throw NSError(domain: "\(key) not found", code: 123)}
        return zoneId
    }

    static func jsonToDict(_ json: String) throws -> [String: Any] {
        if let data = json.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        LogHelper.d("Cannot parse json string \(json)")
        return [:]
    }
    
    static func getClassName(_ instance: Any) -> String {
        return String(String(describing: instance.self).split(separator: ".").last!)
    }
}

extension Dictionary {
    
    static func +(lhs: Self, rhs: Self) -> Self {
        lhs.merging(rhs) { _ , new in new }
    }
    
    func toJsonString() -> String {
        let jsonData = try! JSONSerialization.data(withJSONObject: self)
        return String(data: jsonData, encoding: .utf8)!
    }
}

extension String {
    func splitAndTrim(_ delimiter: String) -> [String] {
        return self.components(separatedBy: delimiter).map{ $0.trimmingCharacters(in: .whitespaces) }
    }
    
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }

    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return String(self[fromIndex...])
    }

    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return String(self[..<toIndex])
    }

    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return String(self[startIndex..<endIndex])
    }
}
