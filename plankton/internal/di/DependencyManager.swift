//
// Created by Funtory on 04/07/2022.
//


class DependencyManager {
    init() {
        DependencyContainer.register(PlanktonPrivacy())
        DependencyContainer.register(RuntimeInfoManager())
        DependencyContainer.register(NetworkManager())
        DependencyContainer.register(FirebaseAnalytics())
        DependencyContainer.register(AppsFlyerAnalytics())
        DependencyContainer.register(AppMetricaAnalytics())
        DependencyContainer.register(PlanktonAnalytics())
        DependencyContainer.register(AnalyticsHandler())
        DependencyContainer.register(PlanktonCycleManager())
        DependencyContainer.register(BannerCycleManager())
        DependencyContainer.register(InterstitialCycleManager())
        DependencyContainer.register(RewardedCycleManager())
        DependencyContainer.register(PlanktonAd())
        DependencyContainer.register(PlanktonRemoteConfig())
        DependencyContainer.register(PlanktonRateApp())
    }
}
