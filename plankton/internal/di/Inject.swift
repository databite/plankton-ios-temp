//
// Created by Funtory on 04/07/2022.
//


@propertyWrapper
public struct Inject<T: Injectable> {
    public var wrappedValue: T

    public init() {
        self.wrappedValue = DependencyContainer.resolve()
    }
}
