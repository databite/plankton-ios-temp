//
//  Unity.swift
//  MyFramework
//
//  Created by Funtory on 3/9/1401 AP.
//

import Foundation
 
@objc public protocol UnityProtocol: AnyObject {

    func initialize()
    
    // -------------------------------------------------------------
    // ---------------------------- ADS ----------------------------
    // -------------------------------------------------------------
    
    func isReady(_ type: String) -> Bool
    
    func isOnline() -> Bool
    
    func show(_ type: String, _ placement: String)
    
    func hide(_ type: String)

    func showMediationTestSuite()
    
    // -------------------------------------------------------------
    // ------------------------- ANALYTICS -------------------------
    // -------------------------------------------------------------

    func setUserProperty(_ key: String, _ value: String)
    
    func logEvent(_ provider: String, _ eventName: String, _ params: String)
    
    // -------------------------------------------------------------
    // ----------------------- REMOTE CONFIG -----------------------
    // -------------------------------------------------------------
    
    func setRemoteConfigMinimumFetchInterval(_ interval: Int64)
    
    func fetchRemoteConfig()
    
    func getRemoteConfigValue(_ key: String, _ defaultValue: String) -> String
    
    // -------------------------------------------------------------
    // ------------------------- RATE APP --------------------------
    // -------------------------------------------------------------
    
    func rateApp()
    
    // -------------------------------------------------------------
    // -------------------------- PRIVACY --------------------------
    // -------------------------------------------------------------
    
    func isGdprConsentRequired() -> String
    
    func setGdprConsent(_ accepted: Int)
}

public class PlanktonFramework: NSObject {

    private var plankton = Plankton()
    
    @objc public static let shared: PlanktonFramework = PlanktonFramework()
}


// MARK: - Unity Bridge
// Exposing all the methods that are visible in Unity.
@objc extension PlanktonFramework: UnityProtocol {

    public func initialize() {
        plankton.initialize()
    }

    // -------------------------------------------------------------
    // ---------------------------- ADS ----------------------------
    // -------------------------------------------------------------
    
    public func isReady(_ type: String) -> Bool {
        plankton.ad.isReady(type: type)
    }
    
    public func isOnline() -> Bool {
        plankton.ad.isOnline()
    }
    
    public func show(_ type: String, _ placement: String) {
        plankton.ad.show(type: type, placement: placement)
    }
    
    public func hide(_ type: String) {
        plankton.ad.hide(type: type)
    }

    public func showMediationTestSuite() {
        plankton.ad.showMediationTestSuite()
    }

    // -------------------------------------------------------------
    // ------------------------- ANALYTICS -------------------------
    // -------------------------------------------------------------
    public func setUserProperty(_ key: String, _ value: String) {
        plankton.analytics.setUserProperty(key, value)
    }

    public func logEvent(_ provider: String, _ eventName: String, _ params: String) {
        plankton.analytics.logEvent(provider, eventName, params)
    }

    // -------------------------------------------------------------
    // ----------------------- REMOTE CONFIG -----------------------
    // -------------------------------------------------------------
    
    public func setRemoteConfigMinimumFetchInterval(_ interval: Int64){
        plankton.remoteConfig.setRemoteConfigMinimumFetchInterval(interval)
    }
    
    public func fetchRemoteConfig(){
        plankton.remoteConfig.fetchRemoteConfig()
    }
    
    public func getRemoteConfigValue(_ key: String, _ defaultValue: String) -> String {
        return plankton.remoteConfig.getRemoteConfigValue(key, defaultValue)
    }
    
    // -------------------------------------------------------------
    // ------------------------- RATE APP --------------------------
    // -------------------------------------------------------------
    
    public func rateApp() {
        plankton.rateApp.requestReview()
    }
    
    // -------------------------------------------------------------
    // -------------------------- PRIVACY --------------------------
    // -------------------------------------------------------------
    
    public func isGdprConsentRequired() -> String {
        return plankton.privacy.isGdprConsentRequired()
    }
    
    public func setGdprConsent(_ accepted: Int) {
        plankton.privacy.setGdprConsent(accepted == 0 ? false : true)
    }
}
