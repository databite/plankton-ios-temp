//
//  UnityMessaging.swift
//  MyFramework
//
//  Created by Funtory on 3/16/1401 AP.
//


class UnityMessaging {
    
    private static let UNITY_GAME_OBJECT_NAME = "Plankton"
    
    private static let TRUE = "true"
    private static let FALSE = "false"
    
    static func sendMessage(_ methodName: String, _ message: String = ""){
        UnitySendMessage(UNITY_GAME_OBJECT_NAME, methodName, message)
        LogHelper.d("unitySendMessage ---> \(methodName): \(message)")
    }
    
    static var ad = Ad()
    static var remoteConfig = RemoteConfig()
    static var rateApp = RateApp()
    
    class Ad {
        
        private let UNKNOWN = "unknown"

        func onAdClosed(_ isDisplayed: Bool, _ isRewardEarned: Bool){
            let result = [
                "displayed": isDisplayed,
                "reward_earned": isRewardEarned
            ]
            sendMessage("OnAdClosed", result.toJsonString())
        }

        private func dropPrefix(_ text: String, _ prefix: String) -> String {
            if text.hasPrefix(prefix){
                return String(text.dropFirst(prefix.count))
            } else {
                return text
            }
        }
    }
    
    class RemoteConfig {
        
        func onRemoteConfigFetched(_ data: String){
            sendMessage("OnRemoteConfigFetched", data)
        }
        
        func OnRemoteConfigFetchFailed(){
            sendMessage("OnRemoteConfigFetchFailed")
        }
    }
    
    class RateApp {
        func onRateAppResult(){
            sendMessage("OnRateAppResult", TRUE)
        }
    }
}
