//
// Created by Funtory on 6/20/2022 AD.
//

class UserDefaultsHelper {

    private static let KEY_PREFIX = "Plankton_"

    private static let defaults = UserDefaults.standard

    static func setValue(_ key: String, _ data: Any) {
        defaults.set(data, forKey: getKey(key))
    }

    static func getString(_ key: String, _ defaultValue: String) -> String {
        defaults.string(forKey: getKey(key)) ?? defaultValue
    }

    static func getInt(_ key: String) -> Int {
        defaults.integer(forKey: getKey(key))
    }

    static func getBool(_ key: String) -> Bool {
        defaults.bool(forKey: getKey(key))
    }
    
    static func setStruct<T: Codable>(_ key: String, data: T){
        do {
            let encoded = try JSONEncoder().encode(data)
            defaults.set(encoded, forKey: getKey(key))
        } catch {
            LogHelper.e(error.localizedDescription)
        }
    }
    
    static func getStruct<T: Codable>(_ key: String, _ type: T.Type) -> T? {
        var result: T? = nil
        if let data = UserDefaults.standard.data(forKey: getKey(key)) {
            do {
                result = try JSONDecoder().decode(T.self, from: data)
            } catch {
                LogHelper.e(error.localizedDescription)
            }
        }
        return result
    }

    private static func getKey(_ key: String) -> String {
        KEY_PREFIX + key
    }
}
