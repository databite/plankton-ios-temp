//
//  LogHelper.swift
//  plankton
//
//  Created by Funtory on 3/18/1401 AP.
//

import os.log

class LogHelper {
    
    private static let LOG_PREFIX = "plankton_debug"
    
    private static let logger = OSLog(subsystem: "Plankton", category: LOG_PREFIX)
    
    static func d(_ message: String){
        os_log("%@", log: logger, type: .debug, message)
    }
    
    static func e(_ message: String){
        os_log("%@", log: logger, type: .error, message)
    }
}
