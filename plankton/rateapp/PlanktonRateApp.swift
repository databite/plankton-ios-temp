//
//  PlanktonRateApp.swift
//  plankton
//
//  Created by Funtory on 26/07/2022.
//

import StoreKit

public class PlanktonRateApp: Injectable {
    
    public func requestReview(){
        LogHelper.d("Calling requestReview() ...")
        SKStoreReviewController.requestReview()
        UnityMessaging.rateApp.onRateAppResult()
    }
    
}
