//
//  plankton.h
//  plankton
//
//  Created by Funtory on 3/17/1401 AP.
//

#import <Foundation/Foundation.h>
#import "TenjinSDK.h"

//! Project version number for plankton.
FOUNDATION_EXPORT double planktonVersionNumber;

//! Project version string for plankton.
FOUNDATION_EXPORT const unsigned char planktonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <plankton/PublicHeader.h>


extern void UnitySendMessage(const char *, const char *, const char *);
