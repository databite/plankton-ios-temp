//
// Created by Funtory on 27/07/2022.
//

import AppLovinSDK
import Foundation
import SwiftUI

public class PlanktonPrivacy: Injectable {
    
    private let GdprStateUserDefaultsKey = "GdprConsentState"
    
    private let MAX_RETRY_COUNT = 5
    
    private let CONNECTION_TIMEOUT = 2.0 // seconds

    private let REGION_EUROPE = "Europe"
    
    private let IP_INFO_URL = "https://ipinfo.io/json"
    
    private var gdprConsentState = ConsentState.Unknown
    
    private var isGettingRegion = false
    
    func initialize() {
        let consentState = readGdprConsentState()
        if consentState == ConsentState.Unknown.rawValue {
            getUserRegion()
        } else {
            setConsentState(ConsentState.init(rawValue: consentState)!)
        }
    }

    public func isGdprConsentRequired() -> String {
        if #available(iOS 14.5, *) {
            LogHelper.d("IsGdprConsentRequired called. state: Not Required (iOS > 14.5)")
            return ConsentState.NotRequired.rawValue
        }
        if gdprConsentState == ConsentState.Unknown && !isGettingRegion {
            getUserRegion()
        }
        LogHelper.d("IsGdprConsentRequired called. state: \(gdprConsentState)")
        return gdprConsentState.rawValue
    }

    public func setGdprConsent(_ accepted: Bool) {
        LogHelper.d("Setting GDPR consent. Accepted=\(accepted)")
        ALPrivacySettings.setHasUserConsent(accepted)
    }

    private func getUserRegion(_ tryCount: Int = 0) {
        if tryCount > MAX_RETRY_COUNT{
            LogHelper.d("Failed to determine region! => User is subjected to GDPR")
            setConsentState(ConsentState.Required)
            isGettingRegion = false
            return
        }
        
        LogHelper.d("Getting user region... (try number \(tryCount))")
        isGettingRegion = true
        let request = createRegionRequest()

        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                do {
                    try self.processIpInfoResponse(data)
                    self.isGettingRegion = false
                } catch {
                    LogHelper.d("JSON error: \(error.localizedDescription)")
                    self.getUserRegion(tryCount + 1)
                }
            } else if let error = error {
                LogHelper.d("HTTP Request Failed \(error)")
                self.getUserRegion(tryCount + 1)
            }
        }
        
        task.resume()
    }
    
    private func createRegionRequest() -> URLRequest {
        var request = URLRequest(url: URL(string: IP_INFO_URL)!)
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.timeoutInterval = CONNECTION_TIMEOUT
        return request
    }
    
    private func processIpInfoResponse(_ data: Data) throws {
        let json = try JSONDecoder().decode([String: String].self, from: data)
        let region = self.extractRegionFromJson(json)
        if region == REGION_EUROPE {
            self.setConsentState(ConsentState.Required)
            LogHelper.d("User is subjected to GDPR")
        }
        else{
            self.setConsentState(ConsentState.NotRequired)
            LogHelper.d("User is NOT subjected to GDPR")
        }
    }
    
    private func extractRegionFromJson(_ responseJson: [String: String]) -> String {
        let region = responseJson["timezone"]?.components(separatedBy: "/").first ?? "Unknown"
        LogHelper.d("User region is \(region)")
        return region
    }
    
    private func setConsentState(_ state: ConsentState) {
        LogHelper.d("Setting ConsentState: \(state.rawValue)")
        gdprConsentState = state
        writeGdprConsentState(state)
    }
    
    private func readGdprConsentState() -> String {
        return UserDefaultsHelper.getString(GdprStateUserDefaultsKey, ConsentState.Unknown.rawValue)
    }
    
    private func writeGdprConsentState(_ state: ConsentState) {
        UserDefaultsHelper.setValue(GdprStateUserDefaultsKey, state.rawValue)
    }
}
